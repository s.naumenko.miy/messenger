# Main goal

Create an imitator of a messenger.

## Stack

HTML5, SASS, JavaScript, Typescript
React, React-redux, React-router, fetch,
firebase, authorization, moment

[DEMO](https://serhii-naumenko.github.io/messenger/)
